# dota6.83 - dota2 custom game

#### Description
An attempt to migrate dota 6.83's balence.

Features:
- All Pick & Random Draft. Enter '-rd' in public chat to enable Random Draft mode, "-ap" to revert.
- Enter '-repick' in AP mode, during hero select and strategy time to repick hero. -100 gold.
- Remove talents, Ags shard, and new heroes introduced after Monkyey King
- Cap hero level to 25, and adjust experience table
- Adjust items and remove neutral items. Lifesteal, mask of madness, helm of dom, satanic, deso, maelstrom, Mjollnir are orbs.
- Use 6.83 map, removes flag bearer creep, removes tower aura.
- Adjust attribute bonus (health gain, regen for strength and mana gain, regen for intellect, base health and mana)
- Adjust neutral creep's experience & gold. Spawn first wave of neutral creeps 30 seconds into the game
- Hero kills gives additional 100 gold and 50 experience. Hero kill assists gold and exp gain adds comeback factor.
- Courier needs to be bought and shared with it's ability. Courier upgrades to flying at level 4, doesn't increase its speed or health with time
- Backpack cooldown to 15s
- Remove melee hero's innate block. Stout sheild can be bought.
- Rewrite TD. Check TD cooldown at the fountain.
- Disable Scan
- Bounty rune now give personal gold and experience, and not to the team.
- Randoming hero pick in AP mode gives 250 extra gold
- No free TP after death
- Side lane creeps meet further from safe tier1 tower
- Being attacked by creeps stops clarity and flask
- Increase heroes attack animation point and reduce turn rate

Hero ability changes:
- Adjust some hero skills' mana cost, CD & damage
- Huskar: Change first ability to health regen, Burning Spear Orb
- Clinkz: Change first ability to strafe, Searing Arrows Orb, removes Aghs
- Drow: Replace ability 1 with trueshot aura
- Viper: Change nethertoxin to be a passive skill, Poison attack is Orb
- Wraith King: Remove skeleton, change crit to percentage chance and removes Aghs
- Zuus: Replace jump with Static Field, Aghs increases ult damage
- Ursa: Stomp no hop, Aghs increases ult CD to 70s
- Puck: Silence no target/movement
- Antimage: No shield active, Mana Break Orb, remove Aghs
- Sniper: No aim(third ability) active
- Techies: Reactive 1st skill stackable, 2nd skill statis trap, Suicide suicides, ultimate remote bomb. Aghs increate ulti damage.
- Tinker: 3rd skill march of machines
- Troll: ulti increase attack speed for all allied heroes, 1st skill bashes
- Meepo: 3rd skill damage and slow, aghs +1 Meepo
- Lich: 2nd skill frost sheild, 3rd skill dark ritual, Aghs ult + damage & remove bounce limit
- Pudge:  3rd skill magic resist passive, Rot suicide, Aghs reduce hook's CD
- Chaos Knight: crit no life steal
- Slardar: Bash to percentage chance and remove aghs
- Mirana: Jump remove changes
- Treant: 1st skill to natures guise
- Shadow Fiend： Aghs no health recovery
- Jugg: Aghs increase ulti duration
- Axe: Aghs increase ulti damage, ult no permanent armor gain
- QOP: Aghs increase ulti damage, reduce cooldown
- Clockwork: Aghs reduce ult cooldown
- Lina: Aghs increase ult cast range, pure damage
- Void: Aghs reduce ult CD
- Gyro: Aghs global ult, + damage
- Visage: Aghs +1 bird
- Earthshaker: Aghs increase ult damage
- Tiny: 3rd skill to craggy, Aghs gives tree grab with unlimited attack counts
- Riki: 2nd skill invis, 3rd skill back stab, ult blink, removes Aghs
- Enchantress: 1st skill passive, Impetus Ult Orb, Aghs increases attack range
- Necro: 2nd skill heartstopper, 3rd skill passive regen, Aghs increases ult damage, no buyback, no permanent health and mana regen
- Brood: 1st skill spiderlings, 3rd skill paralyzing passive, ult life steal, remove Aghs
- Slark: No permanent agility gain, remove Aghs
- Lion: No permanent ult damage gain
- Lone Druid: 2nd skill remove life steal
- Life Stealer: 1st skill add attack speed, 2nd skill life steal based on current health, 3rd skill tear wounds, remove Aghs
- Lycan: 3rd ability increase damage and attack speed, remove Aghs
- Morphling: Ult creates illusion, remove Aghs
- OD: Orb
- Silencer: Orb
- KoTL: Illuminate channel for 2345 seconds, Mana Leak, Aghs adds recovery to Illuminate and Recall
- Abaddon: Coil can suicide, remove Aghs
- Medusa drains mana, remove Aghs
- Omni Repel magic immune, Degen Aura
- Remove Aghs for following heroes: Centuar, Beast Master, Nyx, TA, VS, PA, PL, Spectre, TB, Leshrac, Sven, Kunka, Night Stalker, Death Prophet, Tide, Ember, Mag, DK

mod is open-source ：https://gitee.com/bobo1993324/dota6.83
Bug report at：https://gitee.com/bobo1993324/dota6.83/issues
Thanks to SpellLibrary for implementation of some of the items：https://github.com/Pizzalol/SpellLibrary

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
