-------------------------------------------------------------------------------
--- AUTHOR: Nostrademous
--- GITHUB REPO: https://github.com/Nostrademous/Dota2-FullOverwrite
-------------------------------------------------------------------------------

require( GetScriptDirectory().."/constants" )
require( GetScriptDirectory().."/team_think" )
require( GetScriptDirectory().."/hero_think" )

local gHeroVar = require( GetScriptDirectory().."/global_hero_data" )
local utils = require( GetScriptDirectory().."/utility" )

local function setHeroVar(bot, var, value)
    gHeroVar.SetVar(bot:GetPlayerID(), var, value)
end

local function getHeroVar(bot, var)
    return gHeroVar.GetVar(bot:GetPlayerID(), var)
end

local noneMode      = dofile( GetScriptDirectory().."/modes/none" )
local wardMode      = dofile( GetScriptDirectory().."/modes/ward" )
local shopMode      = dofile( GetScriptDirectory().."/modes/shop" )
local roamMode      = dofile( GetScriptDirectory().."/modes/roam" )
local laningMode    = dofile( GetScriptDirectory().."/modes/laning" )
local runeMode      = dofile( GetScriptDirectory().."/modes/runes" )
local jungleMode    = dofile( GetScriptDirectory().."/modes/jungling" )
local retreatMode   = dofile( GetScriptDirectory().."/modes/retreat" )
local evasionMode   = dofile( GetScriptDirectory().."/modes/evasion" )
local roshanMode    = dofile( GetScriptDirectory().."/modes/roshan" )
--local shrineMode    = dofile( GetScriptDirectory().."/modes/shrine" )
local fightMode     = dofile( GetScriptDirectory().."/modes/fight" )
local pushLaneMode  = dofile( GetScriptDirectory().."/modes/pushlane" )
local defendLaneMode= dofile( GetScriptDirectory().."/modes/defendlane" )
local defendAllyMode= dofile( GetScriptDirectory().."/modes/defendally" )

local freqTeamThink = 1 -- 0.25
local lastTeamThink = {-1000.0, -1000.0}

local playerAssignment = {{},{}}
local playerActionQueues = {}

local X = {}

function X.getLastTeamThink(bot)
	if bot:GetTeam() == DOTA_TEAM_GOODGUYS then
		return lastTeamThink[1]
	else
		return lastTeamThink[2]
	end
end

function X.setLastTeamThink(bot, val)
	if bot:GetTeam() == DOTA_TEAM_GOODGUYS then
		lastTeamThink[1] = val
	else
		lastTeamThink[2] = val
	end
end

function X.getPlayerAssignment(bot) 
	if bot:GetTeam() == DOTA_TEAM_GOODGUYS then
		return playerAssignment[1]
	else
		return playerAssignment[2]
	end
end

function X.MainThink(bot)
    
    -- Exercise TeamThink() at coded frequency
    if GameTime() > X.getLastTeamThink(bot) then
        X.TeamThink(bot)
        X.setLastTeamThink(bot, GameTime() + freqTeamThink)
    end
    
    -- Exercise individual Hero think at every frame (if possible).
    -- HeroThink() will check assignments from TeamThink()
    -- for that individual Hero that it should perform, if any.
    return X.HeroThink(bot)
end

local lastLaneAssignmentThink = {-100, -100, -100}
local function getLastLaneAssignmentThink(bot) return lastLaneAssignmentThink[bot:GetTeam()] end
local function setLastLaneAssignmentThink(bot, curTime) lastLaneAssignmentThink[bot:GetTeam()] = curTime end
function X.TeamThink(bot)
    --utils.myPrint("TeamThink()")
    local listAllies = bot:GetUnitList(UNIT_LIST_ALLIED_HEROES)
    for _, ally in pairs(listAllies) do
        if not ally:IsControllableByAnyPlayer() and (X.getPlayerAssignment(bot)[ally:GetPlayerID()] == nil or not ally:IsAlive()) then
            utils.myPrint("TeamThink() - clearing playerAssignment["..ally:GetPlayerID().."]")
            X.getPlayerAssignment(bot)[ally:GetPlayerID()] = {}
        end
    end
    
	-- In laning stage ( < 10 minutes ), reassign bot lanes based on player positions
	-- Lane assignment think every 10 seconds
	local currentTime = GameRules:GetDOTATime(false, false)
	if currentTime < 600 and currentTime > getLastLaneAssignmentThink(bot) + 5 then
		print(bot:GetName() .. " Team thinks " .. bot:GetTeam())
		-- If a lane has 3 heroes, assign bot to another lane
		--             top, mid, bot
		lanesToHero = {{},  {},  {}}
		for _, ally in pairs(listAllies) do
			if not ally:IsControllableByAnyPlayer() then
				-- bot player, get lane based on CurLane Value
				local curLane = getHeroVar(ally, "CurLane")
				if curLane <= 3 and curLane > 0 then
					table.insert(lanesToHero[curLane], ally)
				end
			else
				--TODO guesses the heroes lane
                if currentTime < 25 then
                    table.insert(lanesToHero[2], ally)
                else
				    -- not bot player, iterate lane front, assign lane if hero is arround
				    for lane=LANE_TOP, LANE_BOT do
				    	-- if creep is behind lane, skip
				    	local creepLocationAmount = bot:GetLaneFrontAmount(bot:GetTeam(), lane, true)
				    	local towerLocation = bot:GetLaneFrontAmount(bot:GetTeam(), lane, false)
				    	if towerLocation < creepLocationAmount + 0.01 then
				    		local creepLocation = bot:GetLocationAlongLane(lane, creepLocationAmount)
				    		if (creepLocation - ally:GetAbsOrigin()):Length2D() < 1500 then
				    			table.insert(lanesToHero[lane], ally)
				    			break
				    		end
				    	end
				    end
                end
			end
		end
		print("--------")
		for i=1,3 do
			print("Lane " .. i)
			for j=1,#lanesToHero[i] do
				print(lanesToHero[i][j]:GetName())
			end
		end

		-- If hero count >= 3, then find one bot that has higher farm priority to assign another lane.
		for i=1,3 do
            local botInLane = nil
            for j=1,#lanesToHero[i] do
                if not lanesToHero[i][j]:IsControllableByAnyPlayer() then
                    botInLane = lanesToHero[i][j]
                    break
                end
            end
			if #lanesToHero[i] >= 3 then
				print("Find lane with 3 heroes: " .. i)
				local laneTooMany = i
				local botHeroWithPriority = nil
				for i=1,#lanesToHero[laneTooMany] do
					if not lanesToHero[laneTooMany][i]:IsControllableByAnyPlayer() then
						if not botHeroWithPriority then
							botHeroWithPriority = lanesToHero[laneTooMany][i]
						else
							if utils.IsCore(lanesToHero[laneTooMany][i]) then
								botHeroWithPriority = lanesToHero[laneTooMany][i]
							end
						end
					end
				end
				if botHeroWithPriority then
					-- Find bots assignment in other lane, find one lane that has no bot assigned, and assign new bot that lane.
					local moveToLane = 0
					for i=1,3 do
						if #lanesToHero[i] == 0 then
							moveToLane = i
						end
					end
					-- move the hero to the other side lane with one
					if moveToLane == 0 then
						moveToLane = 1
						if laneTooMany == 1 then
							moveToLane = 3
						end
					end
					setHeroVar(botHeroWithPriority, "CurLane", moveToLane)
					print("Moving bot hero ".. botHeroWithPriority:GetName() .. " to lane "..moveToLane)
				end
				break
			elseif #lanesToHero[i] == 0 then
				print("Find lane with no hero: " .. i)
				-- Find a lane wit 2 heroes at least 1 bot, assign that bot(Prefer support) to the empty lane
				for j=1,3 do
					if j ~= i then
						if #lanesToHero[j] > 1 then
							local botHero = nil
							for k=1,#lanesToHero[j] do
								if not lanesToHero[j][k]:IsControllableByAnyPlayer() then
									if not botHero or utils.IsCore(botHero) then
										botHero = lanesToHero[j][k]
									end
								end
							end
							if botHero then
								setHeroVar(botHero, "CurLane", i)
								print("Moving bot hero"..botHero:GetName().." to lane "..i)
								break;
							end
						end
					end
				end
				break
			-- if 2 heroes in midlane, one is player, move the other bot to side lane
            elseif i == 2 and botInLane and #lanesToHero[2] > 1 then
                -- Move bot in lane to another lane
				print("Find mid lane with 2 hero")
                local sideLaneWithLeastHeroes = 1
                if #lanesToHero[1] > #lanesToHero[3] then
                    sideLaneWithLeastHeroes = 3
                end
                setHeroVar(botInLane, "CurLane", sideLaneWithLeastHeroes)
				print("Moving bot hero"..botInLane:GetName().." to lane "..sideLaneWithLeastHeroes)
                break
			end
		end

		setLastLaneAssignmentThink(bot, currentTime)
	end
    -- Record all global information used by individual heroes
    -- Example: AOE zones 
    local badAOEZones = {}
    -- NOTE: an aoe will be table with { "location", "ability", "caster", "radius", "playerid" }.  
    for _, aoe in pairs(GetAvoidanceZones()) do
        if aoe.caster:GetTeam() ~= bot:GetTeam() or aoe.ability == "faceless_void_chronosphere" then
            table.insert(badAOEZones, aoe)
        end
    end
    gHeroVar.SetGlobalVar("AOEZones", badAOEZones)
    
    -- Example: Incoming Projectiles
    local badProjectiles = {}
    -- NOTE: a projectile will be a table with { "location", "ability", "velocity", "radius", "playerid" }
    for _, projectile in pairs(GetLinearProjectiles()) do
        if projectile.playerid == nil or GetTeamForPlayer(projectile.playerid) == utils.GetOtherTeam() then
            table.insert(badProjectiles, projectile)
        end
    end
    gHeroVar.SetGlobalVar("BadProjectiles", badProjectiles)

    -- This is at top as all item purchases are Immediate actions,
    -- and therefore won't affect any other decision making.
    -- Intent is to smartly determine when we should use our Glyph
    -- to protect our towers.
	-- TODO
    --team_think.ConsiderGlyphUse()
    
    -- This is at top as all item purchases are Immediate actions,
    -- and therefore won't affect any other decision making.
    -- Intent is to smartly determine which heroes should purchases
    -- Team items like Tome of Knowledge, Wards, Dust/Sentry, and
    -- even stuff like picking up Gem, Aegis, Cheese, etc.
    --team_think.ConsiderTeamWideItemAcquisition(X.getPlayerAssignment(bot))

    -- This is at top as all courier actions are Immediate actions,
    -- and therefore won't affect any other decision making.
    -- Intent is to make courier use more efficient by aligning
    -- the purchases of multiple localized heroes together.
    --team_think.ConsiderTeamWideCourierUse()

    -- This is a fight orchestration evaluator. It will determine,
    -- based on the global picture and location of all enemy and
    -- friendly units, whether we should pick a fight, whether in
    -- the middle of nowhere, as part of a push/defense of a lane,
    -- or even as part of an ally defense. All Heroes involved will
    -- have their actionQueues filled out by this function and
    -- their only responsibility will be to do those actions. Note,
    -- heroes with Global skills (Invoker Sun Strike, Zeus Ult, etc.)
    -- can be part of this without actually being present in the area.
    --team_think.ConsiderTeamFightAssignment(playerActionQueues)
    
    -- Determine which lanes should be pushed and which Heroes should
    -- be part of the push.
    --team_think.ConsiderTeamLanePush()
    
    -- Determine which lanes should be defended and which Heroes should
    -- be part of the defense.
    --team_think.ConsiderTeamLaneDefense(bot)
    
    -- Determine which hero (based on their role) should farm where. By
    -- default it is best to probably leave their default lane assignment,
    -- but if they are getting killed repeatedly we could rotate them. This
    -- also considers jungling assignments and lane rotations.
    --team_think.ConsiderTeamFarmDesignation()
    
    -- Determine if we should Roshan and which Heroes should be part of it.
    --team_think.ConsiderTeamRoshan()
    
    -- Determine if we should seek out a specific enemy for a kill attempt
    -- and which Heroes should be part of the kill.
    --team_think.ConsiderTeamRoam()
    
    -- If we see a rune, determine if any specific Heroes should get it 
    -- (to fill a bottle for example). If not, the hero that saw it will 
    -- pick it up. Also consider obtaining Rune vision if lacking.
    -- team_think.ConsiderTeamRune(playerAssignment)
end

function X.HeroThink(bot)
    
    --utils.myPrint("HeroThink()")
    local highestDesireValue = 0.0
    local highestDesireMode = noneMode
    
    local evaluatedDesireValue = BOT_MODE_DESIRE_NONE 
    
    -- Consider incoming projectiles or nearby AOE and if we can evade.
    -- This is of highest importance b/c if we are stunned/disabled we 
    -- cannot do any of the other actions we might be asked to perform.
    --evaluatedDesireValue = hero_think.ConsiderEvading(bot)
    --if evaluatedDesireValue > highestDesireValue then
    --    highestDesireValue = evaluatedDesireValue
    --    highestDesireMode = evasionMode
    --end
    
    -- If we really want to evade ( >= 0.9 ); short-circuit 
    -- consideration of other modes for better performance.
    --if evaluatedDesireValue >= BOT_MODE_DESIRE_VERYHIGH then
    --    return highestDesireMode, highestDesireValue
    --end
    
    -- Fight orchestration is done at a global Team level.
    -- This just checks if we are given a fight target and a specific
    -- action queue to execute as part of the fight.
    evaluatedDesireValue = hero_think.ConsiderAttacking(bot)
    if evaluatedDesireValue > highestDesireValue then
        highestDesireValue = evaluatedDesireValue
        highestDesireMode = fightMode
    end
    
    
    -- Determine if we should retreat. Team Fight Assignements can 
    -- over-rule our desire though. It might be more important for us to die
    -- in a fight but win the over-all battle. If no Team Fight Assignment, 
    -- then it is up to the Hero to manage their safety from global and
    -- tower/creep damage.
    evaluatedDesireValue = hero_think.ConsiderRetreating(bot)
    if evaluatedDesireValue > highestDesireValue then
        highestDesireValue = evaluatedDesireValue
        highestDesireMode = retreatMode
    end
    
    -- If we really want to Attack, Use Shrine or Retreat ( >= 0.75 ); 
    -- short-circuit consideration of other modes for better performance.
    if evaluatedDesireValue >= BOT_MODE_DESIRE_HIGH then
        return highestDesireMode, highestDesireValue
    end
    
    -- Courier usage is done at Team wide level. We can do our own 
    -- shopping at secret/side shop if we are informed that the courier
    -- will be unavailable to use for a certain period of time.
    --evaluatedDesireValue = hero_think.ConsiderSecretAndSideShop(bot)
    --if evaluatedDesireValue > highestDesireValue then
    --    highestDesireValue = evaluatedDesireValue
    --    highestDesireMode = shopMode
    --end
    
    -- The decision is made at Team level. 
    -- This just checks if the Hero is part of the push, and if so, 
    -- what lane.
    evaluatedDesireValue = hero_think.ConsiderPushingLane(bot)
    if evaluatedDesireValue > highestDesireValue then
        highestDesireValue = evaluatedDesireValue
        highestDesireMode = pushLaneMode
    end
    
    -- The decision is made at Team level.
    -- This just checks if the Hero is part of the defense, and 
    -- where to go to defend if so.
    evaluatedDesireValue = hero_think.ConsiderDefendingLane(bot)
    if evaluatedDesireValue > highestDesireValue then
        highestDesireValue = evaluatedDesireValue
        highestDesireMode = defendLaneMode
    end
    
    -- This is a localized lane decision. An ally defense can turn into an 
    -- orchestrated Team level fight, but that will be determined at the 
    -- Team level. If not a fight, then this is just a "buy my retreating
    -- friend some time to go heal up / retreat".
    evaluatedDesireValue = hero_think.ConsiderDefendingAlly(bot)
    if evaluatedDesireValue > highestDesireValue then
        highestDesireValue = evaluatedDesireValue
        highestDesireMode = defendAllyMode
    end
    
    -- Roaming decision are made at the Team level to keep all relevant
    -- heroes informed of the upcoming kill opportunity. 
    -- This just checks if this Hero is part of the Gank.
    --evaluatedDesireValue = hero_think.ConsiderRoam(bot)
    --if evaluatedDesireValue > highestDesireValue then
    --    highestDesireValue = evaluatedDesireValue
    --    highestDesireMode = roamMode
    --end
    --
    ---- The decision if and who should get Rune is made Team wide.
    ---- This just checks if this Hero should get it.
    --evaluatedDesireValue = hero_think.ConsiderRune(bot, X.getPlayerAssignment(bot))
    --if evaluatedDesireValue > highestDesireValue then
    --    highestDesireValue = evaluatedDesireValue
    --    highestDesireMode = runeMode
    --end
    
    ---- The decision to Roshan is done in TeamThink().
    ---- This just checks if this Hero should be part of the effort.
    --evaluatedDesireValue = hero_think.ConsiderRoshan(bot)
    --if evaluatedDesireValue > highestDesireValue then
    --    highestDesireValue = evaluatedDesireValue
    --    highestDesireMode = roshanMode
    --end
    
    ---- Farming assignments are made Team Wide.
    ---- This just tells the Hero where he should go to Jungle.
    --evaluatedDesireValue = hero_think.ConsiderJungle(bot, X.getPlayerAssignment(bot))
    --if evaluatedDesireValue > highestDesireValue then
    --    highestDesireValue = evaluatedDesireValue
    --    highestDesireMode = jungleMode
    --end
    
    -- Laning assignments are made Team Wide for Pushing & Defending.
    -- Laning assignments are initially determined at start of game/hero-selection.
    -- This just tells the Hero which Lane he is supposed to be in.
    evaluatedDesireValue = hero_think.ConsiderLaning(bot, X.getPlayerAssignment(bot))
    if evaluatedDesireValue > highestDesireValue then
        highestDesireValue = evaluatedDesireValue
        highestDesireMode = laningMode
    end
    
    ---- Warding is done on a per-lane basis. This evaluates if this Hero
    ---- should ward, and where. (might be a team wide thing later)
    --evaluatedDesireValue = hero_think.ConsiderWarding(bot, X.getPlayerAssignment(bot))
    --if evaluatedDesireValue > highestDesireValue then
    --    highestDesireValue = evaluatedDesireValue
    --    highestDesireMode = wardMode
    --end
    
    utils.myPrint(bot:GetName(), " HeroThink() " .. highestDesireMode:GetName() .. " " ..  highestDesireValue)
    return highestDesireMode, highestDesireValue
end

return X
