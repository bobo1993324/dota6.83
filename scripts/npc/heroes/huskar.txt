"DOTAAbilities"
{
	"Version"       "1"
	"huskar_berserkers_blood_lua"
	{
		"BaseClass"				"ability_lua"
		"ScriptFile"            "heroes/hero_huskar/berserkers_blood.lua"
		"AbilityBehavior"		"DOTA_ABILITY_BEHAVIOR_PASSIVE"
		"AbilityUnitTargetTeam"	"DOTA_UNIT_TARGET_TEAM_FRIENDLY"
		"AbilityTextureName"	"huskar_berserkers_blood"
		"IsBreakable"			"1"

		"AbilityValues"
		{
			"attack_speed_bonus_per_stack"	"14 16 18 20"
			"resistance_per_stack"			"4 5 6 7"
			"hp_threshold_max"				"3"
		}
	}
}