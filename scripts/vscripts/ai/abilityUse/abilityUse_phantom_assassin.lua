-------------------------------------------------------------------------------
--- AUTHOR: Nostrademous
--- GITHUB REPO: https://github.com/Nostrademous/Dota2-FullOverwrite
-------------------------------------------------------------------------------

BotsInit = require( "game/botsinit" )
local genericAbility = BotsInit.CreateGeneric()

local heroData = require( GetScriptDirectory().."/hero_data" )
local utils = require( GetScriptDirectory().."/utility" )
local gHeroVar = require( GetScriptDirectory().."/global_hero_data" )

require( GetScriptDirectory().."/modifiers" )

function setHeroVar(bot, var, value)
    gHeroVar.SetVar(bot:GetPlayerID(), var, value)
end

function getHeroVar(bot, var)
    return gHeroVar.GetVar(bot:GetPlayerID(), var)
end

local Abilities = {
    heroData.phantom_assassin.SKILL_0,
    heroData.phantom_assassin.SKILL_1,
    heroData.phantom_assassin.SKILL_2,
    heroData.phantom_assassin.SKILL_3
}

local abilityQ = ""
local abilityW = ""
local abilityE = ""
local abilityR = ""

local AvgCritMult   = 0.0
local AttackRange   = 0
local ManaPerc      = 0.0
local HealthPerc    = 0.0
local modeName      = nil

function ConsiderQ(bot)
    if not abilityQ:IsFullyCastable() then
        return BOT_ACTION_DESIRE_NONE, nil
    end

    local daggerCastRange = abilityQ:GetCastRange()
    local daggerDamage = abilityQ:GetAbilityDamage()
    
    -- Try to kill enemy hero
    local WeakestEnemy, HeroHealth = utils.GetWeakestHero(bot, daggerCastRange)
	if modeName ~= "retreat" or (modeName == "retreat" and bot.SelfRef:getCurrentModeValue() < BOT_MODE_DESIRE_VERYHIGH) then
		if utils.ValidTarget(WeakestEnemy) then
			if not modifiers.IsPhysicalImmune(WeakestEnemy) then
				if HeroHealth <= GetActualIncomingDamage(WeakestEnemy, daggerDamage / 2, DAMAGE_TYPE_PURE) then
					return BOT_ACTION_DESIRE_HIGH, WeakestEnemy
				end
			end
		end
	end
    
    -- farming/laning
	if modeName == "jungling" or modeName == "laning" then
        if ManaPerc > 0.5 then            
            local eCreep = gHeroVar.GetNearbyEnemyCreep(bot, daggerCastRange)
            local weakestCreep, weakestCreepHealth = utils.GetWeakestCreep(eCreep)
            if utils.ValidTarget(weakestCreep) then
                local dist = GetUnitToUnitDistance(bot, weakestCreep)
                if dist > 1.25*AttackRange and 
                    weakestCreepHealth < GetActualIncomingDamage(weakestCreep, daggerDamage, DAMAGE_TYPE_PHYSICAL) then
                    return BOT_ACTION_DESIRE_LOW, weakestCreep
                end
            end
            
            -- in lane harass
            if utils.ValidTarget(WeakestEnemy) and weakestCreepHealth > 150 then
                if not modifiers.IsPhysicalImmune(WeakestEnemy) then
                    return BOT_ACTION_DESIRE_LOW, WeakestEnemy
                end
            end
        end
    end
    
    -- If we're seriously retreating, see if we can land a stun on someone who's damaged us recently
	if modeName == "retreat" then
		local tableNearbyEnemyHeroes = gHeroVar.GetNearbyEnemies( bot, daggerCastRange )
		for _, npcEnemy in pairs( tableNearbyEnemyHeroes ) do
			if utils.ValidTarget(npcEnemy) and WasRecentlyDamagedByHero(bot, npcEnemy, 2.0 ) then
				if not modifiers.IsPhysicalImmune( npcEnemy ) then
					return BOT_ACTION_DESIRE_MODERATE, npcEnemy
				end
			end
		end
	end

	-- If we're going after someone
	if modeName == "roam" or modeName == "defendally" or modeName == "fight" then
		local npcEnemy = getHeroVar(bot, "RoamTarget")
        if npcEnemy == nil then npcEnemy = getHeroVar(bot, "Target") end

		if utils.ValidTarget(npcEnemy) then
			if not utils.IsTargetMagicImmune(npcEnemy) and not utils.IsCrowdControlled(npcEnemy) then
				return BOT_ACTION_DESIRE_HIGH, npcEnemy
			end
		end
	end

    return BOT_ACTION_DESIRE_NONE, nil
end

function ConsiderW(bot)
    
    if not abilityW:IsFullyCastable() then
        return BOT_ACTION_DESIRE_NONE, nil
    end

    local phantomStrikeCastRange = abilityW:GetCastRange()
    -- phantom_assassin_phantom_strike adds 4 very fast attacks
    local totalAttackDamage = 4 * bot:GetAttackDamage() * AvgCritMult

    -- Try to kill enemy hero
    local WeakestEnemy, HeroHealth = utils.GetWeakestHero(bot, phantomStrikeCastRange + 300)
	if modeName ~= "retreat" or (modeName == "retreat" and bot.SelfRef:getCurrentModeValue() < BOT_MODE_DESIRE_VERYHIGH) then
		if utils.ValidTarget(WeakestEnemy) then
			if not modifiers.IsPhysicalImmune(WeakestEnemy) then
				if HeroHealth <= GetActualIncomingDamage(WeakestEnemy, totalAttackDamage, DAMAGE_TYPE_PHYSICAL) then
                    setHeroVar(bot, "Target", WeakestEnemy)
					return BOT_ACTION_DESIRE_HIGH, WeakestEnemy
				end
			end
		end
	end

    -- If we're going after someone
	if modeName == "roam" or modeName == "defendally" or modeName == "fight" then
		local npcEnemy = getHeroVar(bot, "RoamTarget")
        if npcEnemy == nil then npcEnemy = getHeroVar(bot, "Target") end

		if utils.ValidTarget(npcEnemy) and not modifiers.IsPhysicalImmune(npcEnemy) then
			return BOT_ACTION_DESIRE_MODERATE, npcEnemy
		end
	end
    
    --phantom_assassin_phantom_strike to escape
    if modeName == "retreat" then
        local nearAllies = gHeroVar.GetNearbyAllies(bot, phantomStrikeCastRange + 100)
        local nearAlliedCreep = gHeroVar.GetNearbyAlliedCreep(bot, phantomStrikeCastRange + 100)
        local combinedList = { unpack(nearAllies), unpack(nearAlliedCreep) }
        if #combinedList > 0 then
            table.sort(combinedList, function(n1,n2) return DistanceFromFountain(n1) < DistanceFromFountain(n2) end)
            return BOT_ACTION_DESIRE_VERYHIGH, combinedList[1]
        end
    end

    return BOT_ACTION_DESIRE_NONE, nil
end


function genericAbility:AbilityUsageThink(bot)
    -- Check if we're already using an ability
    if utils.IsBusy(bot) then return true end

    -- Check to see if we are CC'ed
    if utils.IsUnableToCast(bot) then return false end

    if abilityQ == "" then abilityQ = bot:GetAbilityByName( Abilities[1] ) end
    if abilityW == "" then abilityW = bot:GetAbilityByName( Abilities[2] ) end
    if abilityE == "" then abilityE = bot:GetAbilityByName( Abilities[3] ) end
    if abilityR == "" then abilityR = bot:GetAbilityByName( Abilities[4] ) end

    AttackRange   = bot:Script_GetAttackRange() + bot:BoundingRadius2D()
    ManaPerc      = bot:GetMana()/bot:GetMaxMana()
	HealthPerc    = bot:GetHealth()/bot:GetMaxHealth()
    modeName      = bot.SelfRef:getCurrentMode():GetName()
    
    local modeDesire = Max(0.01, bot.SelfRef:getCurrentModeValue())
    
    local critChance = 0.0
    local critDmg = 0.0
    if abilityR:GetLevel() >= 1 then
        critChance = 0.15
        critDmg = abilityR:GetSpecialValueFor("crit_chance")/100.0
    end
    AvgCritMult = (1 + critChance * critDmg)
    
    -- WRITE CODE HERE --
    local nearbyEnemyHeroes = gHeroVar.GetNearbyEnemies(bot, 1200)
    local nearbyEnemyCreep = gHeroVar.GetNearbyEnemyCreep(bot, 1200)
    
    if #nearbyEnemyHeroes == 0 and #nearbyEnemyCreep == 0 then return false end

    if #nearbyEnemyHeroes == 1 and nearbyEnemyHeroes[1]:GetHealth() > 0 then
        local enemy = nearbyEnemyHeroes[1]
        local dmg, castQueue, castTime, stunTime, slowTime, engageDist = self:nukeDamage( bot, enemy )

        local rightClickTime = stunTime + 0.5*slowTime
        if rightClickTime > 0.5 then
            dmg = dmg + fight_simul.estimateRightClickDamage( bot, enemy, rightClickTime ) * AvgCritMult
        end

        -- magic/physical immunity is already accounted for by nukeDamage()
        if utils.ValidTarget(enemy) and dmg > enemy:GetHealth() then
            local bKill = self:queueNuke(bot, enemy, castQueue, engageDist)
            if bKill then
                setHeroVar(bot, "Target", enemy)
                return true
            end
        end
    end
    
    -- Consider using each ability
	local castQDesire, castQTarget  = ConsiderQ(bot)
	local castWDesire, castWTarget  = ConsiderW(bot)

    if castWDesire >= modeDesire and castWDesire >= castQDesire then
        gHeroVar.HeroUseAbilityOnEntity(bot, abilityW, castWTarget)
        local numAttacks = math.ceil(3/bot:GetSecondsPerAttack(false))
        for i = 1, numAttacks, 1 do
            gHeroVar.HeroQueueAttackUnit(bot, castWTarget, true)
        end
		return true
    end
    
    if castQDesire >= modeDesire then
        gHeroVar.HeroUseAbilityOnEntity(bot, abilityQ, castQTarget)
		return true
    end
    
    return false
end

function genericAbility:nukeDamage( bot, enemy )
    if not utils.ValidTarget(enemy) then return 0, {}, 0, 0, 0 end

    local comboQueue = {}
    local manaAvailable = bot:GetMana()
    local dmgTotal = GetOffensivePower(bot)
    local castTime = 0
    local stunTime = 0
    local slowTime = 0
    local engageDist = 0

    -- WRITE CODE HERE --
    local physImmune = modifiers.IsPhysicalImmune(enemy)
    --local magicImmune = utils.IsTargetMagicImmune(enemy)
    
    if abilityQ:IsFullyCastable() then
    local manaCostQ = abilityQ:GetManaCost(-1)
        if manaCostQ <= manaAvailable then
            if not physImmune then
                manaAvailable = manaAvailable - manaCostQ
                local damageQ = abilityQ:GetSpecialValueFor("base_damage") + abilityQ:GetSpecialValueFor("attack_factor_tooltip") / 100 * bot:GetAttackDamage()
                dmgTotal = dmgTotal + damageQ*AvgCritMult
                castTime = castTime + abilityQ:GetCastPoint()
                slowTime = slowTime + abilityQ:GetDuration()
                engageDist = Min(engageDist, abilityQ:GetCastRange())
                table.insert(comboQueue, abilityQ)
            end
        end
    end
    
    if abilityW:IsFullyCastable() then
        local manaCostW = abilityW:GetManaCost(-1)
        if manaCostW <= manaAvailable then
            if not physImmune then
                manaAvailable = manaAvailable - manaCostW
                dmgTotal = dmgTotal + 4*bot:GetAttackDamage()*AvgCritMult
                castTime = castTime + abilityW:GetCastPoint()
                engageDist = Min(engageDist, abilityW:GetCastRange())
                table.insert(comboQueue, abilityW)
            end
        end
    end

    return dmgTotal, comboQueue, castTime, stunTime, slowTime, engageDist
end

function genericAbility:queueNuke(bot, enemy, castQueue, engageDist)
    if not utils.ValidTarget(enemy) then return false end

    -- WRITE CODE HERE --
    local dist = GetUnitToUnitDistance(bot, enemy)

    -- if out of range, attack move for one hit to get in range
    if dist < engageDist then
        bot:Action_ClearActions(true)
        utils.AllChat("Killing "..utils.GetHeroName(enemy).." softly with my song")
        utils.myPrint("Queue Nuke Damage: ", utils.GetHeroName(enemy))
        for i = #castQueue, 1, -1 do
            local skill = castQueue[i]

            if skill:GetName() == "phantom_assassin_stifling_dagger" then
                gHeroVar.HeroPushUseAbilityOnEntity(bot, skill, enemy)
            elseif skill:GetName() == "phantom_assassin_phantom_strike_datadriven" then
                gHeroVar.HeroPushUseAbilityOnEntity(bot, skill, enemy)
            end
        end
        gHeroVar.HeroQueueAttackUnit( bot, enemy, false )
        bot:ActionQueue_Delay(0.01)
        return true
    end

    return false
end

return genericAbility
