"DOTAAbilities"
{
	"Version"   "1"
	"spirit_breaker_charge_of_darkness_datadriven"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_datadriven"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_UNIT_TARGET | DOTA_ABILITY_BEHAVIOR_DONT_ALERT_TARGET | DOTA_ABILITY_BEHAVIOR_ROOT_DISABLES"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_YES"
		"FightRecapLevel"				"1"
		"AbilityTextureName"			"spirit_breaker_charge_of_darkness"
		"AbilityCastAnimation"			"ACT_DOTA_IDLE"
		"AbilityProcsMagicStick"		"1"
		
		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastPoint"				"0.47"
		"AbilityCastRange"				"0"
	
		// Time		
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"12"
	
		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"100"
	
		// Stats
		//-------------------------------------------------------------------------------------------------------------
		"AbilityModifierSupportValue"	".30" // applies multiple modifiers
	
		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilityValues"
		{
			"movement_speed"		"600 650 700 750"
			"stun_duration"			"1.2 1.6 2.0 2.4"
			"bash_radius"			"300"
			"vision_radius"			"400"
			"vision_duration"		"0.94"
		}

		"precache"
		{
			"particle"	"particles/units/heroes/hero_spirit_breaker/spirit_breaker_charge.vpcf"
			"particle"	"particles/units/heroes/hero_spirit_breaker/spirit_breaker_charge_target.vpcf"
			// custom weapon effect
			"particle"	"particles/units/heroes/hero_spirit_breaker/spirit_breaker_ambient_flame_default.vpcf"
			"particle"	"particles/units/heroes/hero_spirit_breaker/spirit_breaker_ambient_flame_red.vpcf"
		}
	
		"Modifiers"
		{
			"modifier_spirit_breaker_charge_target_vision_datadriven"
			{
				"IsHidden"  "1"
				"Duration"  "1"
				"States"
				{
					"MODIFIER_STATE_PROVIDES_VISION"	"MODIFIER_STATE_VALUE_ENABLED"
				}
			}
		}
		"OnSpellStart"
		{
			"RunScript"
			{
				"ScriptFile"	"heroes/hero_spirit_breaker/charge_of_darkness.lua"
				"Function"		"handleSpellStart"
			}
		}
	}

	"spirit_breaker_nether_strike_datadriven"
	{
		"BaseClass"					 	"ability_datadriven"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_UNIT_TARGET | DOTA_ABILITY_BEHAVIOR_IGNORE_BACKSWING"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitTargetFlags"		"DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_YES"
		"SpellDispellableType"			"SPELL_DISPELLABLE_YES_STRONG"
		"AbilityType"					"DOTA_ABILITY_TYPE_ULTIMATE"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_MAGICAL"
		"FightRecapLevel"				"2"
		"AbilitySound"					"Hero_Spirit_Breaker.NetherStrike.Begin"
		"MaxLevel"					  	"3"
		"RequiredLevel"				 	"6"
		"LevelsBetweenUpgrades"		 	"5"
		"AbilityTextureName"			"spirit_breaker_nether_strike"
		"HasScepterUpgrade"				"1"

		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastPoint"				"1.2"
		"AbilityCastRangeBuffer"		"500"
		"AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_4"

		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"125 150 175"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilityValues"
		{
			"damage"				"150 250 350"
			"fade_time"				"1.0 1.0 1.0"
			"AbilityCastRange"		
			{
				"value"				 	"700"
				"special_bonus_scepter" "+150"
			}		
			"AbilityCooldown"				
			{
				"value"				 	"80 70 60"
				"special_bonus_scepter" "-60 -50 -40"
			}
			"bash_radius"
			{
				"value"				 	"0"
				"special_bonus_scepter" "250"
			}
		}

		"precache"
		{
			"particle"	"particles/units/heroes/hero_spirit_breaker/spirit_breaker_haste_owner.vpcf"
			"particle"	"particles/units/heroes/hero_spirit_breaker/spirit_breaker_greater_bash.vpcf"
		}

		"Modifiers"
		{
			"modifier_spirit_breaker_greater_bash_speed_datadriven"
			{
				"TextureName"   "spirit_breaker_greater_bash"
				"Duration"	  	"3"
				"EffectName"	"particles/units/heroes/hero_spirit_breaker/spirit_breaker_haste_owner.vpcf"
				"Properties"
				{
					"MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE"  "15"
				}
			}
			"modifier_spirit_breaker_nether_target_vision_datadriven"
			{
				"IsHidden"  "1"
				"Duration"  "1.3"
				"States"
				{
					"MODIFIER_STATE_PROVIDES_VISION"	"MODIFIER_STATE_VALUE_ENABLED"
				}
			}
		}

		"OnAbilityPhaseStart"
		{
			"FireSound"
			{
				"EffectName"	"Hero_Spirit_Breaker.NetherStrike.Begin"
				"Target"		"CASTER"
			}
			"ApplyModifier"
			{
				"Target"		"TARGET"
				"ModifierName"	"modifier_spirit_breaker_nether_target_vision_datadriven"
			}
		}

		"OnSpellStart"
		{
			"RunScript"
			{
				"ScriptFile"	"heroes/hero_spirit_breaker/nether_strike.lua"
				"Function"	  	"handleSpellStart"
			}
		}
	}
	"spirit_breaker_empowering_haste_datadriven"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"					"ability_datadriven"
		"AbilityBehavior"			"DOTA_ABILITY_BEHAVIOR_NO_TARGET | DOTA_ABILITY_BEHAVIOR_IMMEDIATE | DOTA_ABILITY_BEHAVIOR_AURA"
		"AbilityUnitTargetTeam"		"DOTA_UNIT_TARGET_TEAM_FRIENDLY"
		"AbilityTextureName"		"spirit_breaker_empowering_haste"
		"AbilityCastAnimation"		"ACT_DOTA_CAST_ABILITY_2"
	
		// Time		
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"			"16"
	
		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilityValues"
		{
			"bonus_movespeed_pct"			"6 10 14 18"
			"aura_radius"					"900"
			"bonus_movespeed_pct_extra"		"3 5 7 9"
			"duration"						"6"
		}
		"precache"
		{
			"particle"						"particles/units/heroes/hero_spirit_breaker/spirit_breaker_haste_owner.vpcf"
		}
		"Modifiers"
		{
			"modifier_spirit_breaker_empowering_haste_aura_datadriven"
			{
				"Passive"				"1"
				"IsHidden"				"1"
				"Aura"					"modifier_spirit_breaker_empowering_haste_datadriven"
				"Aura_Radius"			"%aura_radius"
				"Aura_Teams"			"DOTA_UNIT_TARGET_TEAM_FRIENDLY"
				"Aura_Types"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_CREEP"
			}
			"modifier_spirit_breaker_empowering_haste_datadriven"
			{
				"Attributes"	"MODIFIER_ATTRIBUTE_MULTIPLE"
				"ThinkInterval"	"0.1"
				"EffectName"	"particles/units/heroes/hero_spirit_breaker/spirit_breaker_haste_owner.vpcf"
				"Properties"
				{
					"MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE"	"%bonus_movespeed_pct_extra"
				}
				"OnIntervalThink"
				{
					"RunScript"
					{
						"ScriptFile"	"heroes/hero_spirit_breaker/empowering_haste.lua"
						"Function"		"handleIntervalThink"
					}
				}
			}
		}
	}
	"spirit_breaker_greater_bash_datadriven"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_datadriven"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_MAGICAL"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_YES"
		"AbilityTextureName"			"spirit_breaker_greater_bash"
	
		// Time		
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"1.5"
	
		// Stats
		//-------------------------------------------------------------------------------------------------------------
		"AbilityModifierSupportBonus"	"40"
	
		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilityValues"
		{
				"chance_pct"			"17"
				"damage"				"22 28 34 40"
				"duration"				"1.0 1.2 1.4 1.6"
				"knockback_duration"	"0.5"
				"knockback_distance"	"143 152 158 162"
				"knockback_height"		"50"
				"bonus_movespeed_pct"	"15"
				"movespeed_duration"	"3"
		}
		"precache"
		{
			"particle"					"particles/units/heroes/hero_spirit_breaker/spirit_breaker_greater_bash.vpcf"
		}
		"Modifiers"
		{
			"modifier_spirit_breaker_greater_bash_datadriven"
			{
				"IsHidden"			"1"
				"Passive"			"1"
				"OnAttackLanded"
				{
					"RunScript"
					{
						"ScriptFile"	"heroes/hero_spirit_breaker/greater_bash.lua"
						"Function"		"handleAttackLanded"
					}
				}
			}
			"modifier_spirit_breaker_greater_bash_speed_datadriven"
			{
				"Duration"		"%movespeed_duration"
				"Properties"
				{
					"MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE"	"%bonus_movespeed_pct"
				}
			}
		}
	}
}
