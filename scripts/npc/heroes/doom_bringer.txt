// Rewrite of the Doom Devour ability
// Author: Pizzalol
// Date: January 27, 2016
// Version: 6.86
// Type: Datadriven
// NOTES: You have to add the specific units to the Lua table to be able to steal their abilities
//
// ----- FILE REQUIREMENTS -----
// Script files:
// scripts/vscripts/heroes/hero_doom_bringer/devour.lua
//
// KV files:
// scripts/npc/abilities/doom_bringer/doom_bringer_empty1_datadriven.txt
// scripts/npc/abilities/doom_bringer/doom_bringer_empty2_datadriven.txt
"DOTAAbilities"
{
	"Version"	"1"
	"doom_bringer_devour_datadriven"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_doom_bringer/devour.lua"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_UNIT_TARGET | DOTA_ABILITY_BEHAVIOR_DONT_RESUME_ATTACK"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_CREEP"
		"AbilityUnitTargetFlags"		"DOTA_UNIT_TARGET_FLAG_NOT_CREEP_HERO | DOTA_UNIT_TARGET_FLAG_NOT_ANCIENTS"
		"AbilityTextureName"			"doom_bringer_devour"
		"AbilityProcsMagicStick"		"1"
	
		// Precache
		//-------------------------------------------------------------------------------------------------------------
		"precache"
		{
			"soundfile"			"soundevents/game_sounds_heroes/game_sounds_doombringer.vsndevts"
			"particle"			"particles/units/heroes/hero_doom_bringer/doom_bringer_devour.vpcf"
			// custom weapon effect
			"particle"			"particles/units/heroes/hero_doom_bringer/doom_bringer_ambient_default.vpcf"
			"particle"			"particles/units/heroes/hero_doom_bringer/doom_bringer_ambient_purple.vpcf"
			"particle"			"particles/units/heroes/hero_doom_bringer/doom_bringer_ambient_gold.vpcf"
		}
		
		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastPoint"				"0.3"
	
		// Time		
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"300"
		"AbilityCooldown"				"70 60 50 40"
	
		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"60"
	
		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilityValues"
		{
				"bonus_gold"			"25 50 75 100"
				"health_per_second"		"20"
		}
	
		"Modifiers"
		{
			"modifier_devour_datadriven"
			{
				"IsDebuff"			"1"
	
				"OnDestroy"
				{
					"RunScript"
					{
						"ScriptFile"	"heroes/hero_doom_bringer/devour.lua"
						"Function"		"DevourGold"
					}				
				}
			}
		}
	}
	// Rewrite of the Doom Scorched Earth ability
	// Author: Pizzalol
	// Date: January 27, 2016
	// Version: 6.86
	// Type: Datadriven
	//
	// ----- FILE REQUIREMENTS -----
	// Script files:
	// scripts/vscripts/heroes/hero_doom_bringer/scorched_earth.lua
	"doom_bringer_scorched_earth_datadriven"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_datadriven"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_MAGICAL"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"
		"FightRecapLevel"				"1"
		"AbilityTextureName"			"doom_bringer_scorched_earth"
		"AbilityProcsMagicStick"		"1"
		"AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_2"
	
		// Precache
		//-------------------------------------------------------------------------------------------------------------
		"precache"
		{
			"soundfile"			"soundevents/game_sounds_heroes/game_sounds_doombringer.vsndevts"
			"particle"			"particles/units/heroes/hero_doom_bringer/doom_bringer_scorched_earth_buff.vpcf"
			"particle"			"particles/units/heroes/hero_doom_bringer/doom_scorched_earth.vpcf"
			"particle"			"particles/units/heroes/hero_doom_bringer/doom_bringer_scorched_earth_debuff.vpcf"				
		}
		
		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastPoint"				"0"
	
		// Time		
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"600"
		"AbilityCooldown"				"60 55 50 45"
	
		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"60 65 70 75"
	
		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilityValues"
		{
				"damage_per_second"			"12 18 24 30"
				"radius"					"600"
				"bonus_movement_speed_pct"	"16"
				"duration"					"10 12 14 16"
		}
	
		"OnSpellStart"
		{
			"ApplyModifier"
			{
				"ModifierName"	"modifier_scorched_earth_datadriven"
				"Target" 		"CASTER"
				"Duration"		"%duration"
			}
		}
	
		"Modifiers"
		{
			"modifier_scorched_earth_datadriven"
			{
				"IsBuff"			"1"
				"IsPurgable"		"0"
		
				"OnCreated"
				{
					"AttachEffect"
					{
						"EffectName"		"particles/units/heroes/hero_doom_bringer/doom_scorched_earth.vpcf"
						"EffectAttachType"	"follow_origin"
						"Target"			"TARGET"
	
						"ControlPoints"
						{
							"01"	"%radius 0 0"
						}
					}
				}
		
				"Aura"          	"modifier_scorched_earth_check_datadriven"
				"Aura_Radius"   	"%radius"
				"Aura_Teams"    	"DOTA_UNIT_TARGET_TEAM_FRIENDLY"
				"Aura_Types"    	"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
				"Aura_Flags"    	"DOTA_UNIT_TARGET_FLAG_NONE"
				"Aura_ApplyToCaster" "1"
	
				"ThinkInterval"  "1.0"
				"OnIntervalThink"
				{
					"ActOnTargets"
					{
						"Target"
						{
							"Center"  	"CASTER"
							"Radius" 	"%radius"
							"Teams" 	"DOTA_UNIT_TARGET_TEAM_ENEMY"
							"Types" 	"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
						}
					
						"Action"    
						{
							"Damage"
							{
								"Target"		"TARGET"
								"Type"			"DAMAGE_TYPE_MAGICAL"
								"Damage"		"%damage_per_second"
							}
						}
					}
				}
			}
	
			"modifier_scorched_earth_check_datadriven"
			{
				"IsHidden"	"1"
	
				"OnCreated"
				{
					"RunScript"
					{
						"ScriptFile"	"heroes/hero_doom_bringer/scorched_earth.lua"
						"Function"		"ScorchedEarthCheck"
						"modifier"		"modifier_scorched_earth_buff_datadriven"
					}
				}
	
				"OnDestroy"
				{
					"RemoveModifier"
					{
						"ModifierName"	"modifier_scorched_earth_buff_datadriven"
						"Target" 		"TARGET"
					}
				}
			}
	
			"modifier_scorched_earth_buff_datadriven"
			{
				"IsHidden"	"1"
	
				"OnCreated"
				{
					"FireSound"
					{
						"EffectName"	"Hero_DoomBringer.ScorchedEarthAura"
						"Target" 		"TARGET"
					}
				}
	
				"OnDestroy"
				{
					"RunScript"
					{
						"ScriptFile"	"heroes/hero_doom_bringer/scorched_earth.lua"
						"Function"		"StopSound"
						"sound"			"Hero_DoomBringer.ScorchedEarthAura"
					}
				}
	
				"Properties"
				{
					"MODIFIER_PROPERTY_HEALTH_REGEN_CONSTANT" 		"%damage_per_second"
				    "MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE" 	"%bonus_movement_speed_pct" 
				}
			}
		}
	}
}
