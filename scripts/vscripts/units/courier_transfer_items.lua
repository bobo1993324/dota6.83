ITEM_RECIPE_RULES = {
	{"item_poor_mans_shield", "item_slippers", "item_slippers", "item_stout_shield" },
	{"item_necronomicon_datadriven", "item_recipe_necronomicon_datadriven", "item_belt_of_strength", "item_staff_of_wizardry" },
	{"item_necronomicon_2_datadriven", "item_recipe_necronomicon_datadriven", "item_necronomicon_datadriven" },
	{"item_necronomicon_3_datadriven", "item_recipe_necronomicon_datadriven", "item_necronomicon_2_datadriven" },
	{"item_ring_of_aquila_lua", "item_wraith_band_datadriven", "item_ring_of_basilius_datadriven"},
	{"item_magic_wand", "item_recipe_magic_wand", "item_branches", "item_branches", "item_branches", "item_magic_stick"},
	{"item_travel_boots_datadriven", "item_recipe_travel_boots_datadriven", "item_boots"},
	{"item_phase_boots", "item_boots", "item_blades_of_attack", "item_blades_of_attack"},
	{"item_hand_of_midas_datadriven", "item_recipe_hand_of_midas_datadriven", "item_gloves"},
	{"item_oblivion_staff_datadriven", "item_quarterstaff", "item_sobi_mask_datadriven", "item_robe"},
	{"item_pers_datadriven", "item_ring_of_health", "item_void_stone_datadriven"},
	{"item_wraith_band_datadriven", "item_recipe_wraith_band_datadriven", "item_circlet", "item_slippers"},
	{"item_null_talisman_datadriven", "item_recipe_null_talisman_datadriven", "item_circlet", "item_mantle"},
	{"item_mekansm_2", "item_recipe_mekansm_2", "item_headdress_datadriven", "item_buckler_2"},
	{"item_vladmir_2", "item_recipe_vladmir_2", "item_ring_of_regen", "item_ring_of_basilius_datadriven", "item_lifesteal_datadriven"},
	{"item_buckler_2", "item_recipe_buckler_2", "item_chainmail", "item_branches"},
	{"item_ring_of_basilius_datadriven", "item_sobi_mask_datadriven", "item_ring_of_protection"},
	{"item_pipe_datadriven", "item_recipe_pipe_datadriven", "item_hood_of_defiance_datadriven", "item_headdress_datadriven"},
	{"item_urn_of_shadows_datadriven", "item_recipe_urn_of_shadows_datadriven", "item_sobi_mask_datadriven", "item_gauntlets", "item_gauntlets"},
	{"item_headdress_datadriven", "item_recipe_headdress_datadriven", "item_ring_of_regen", "item_branches"},
	{"item_sheepstick", "item_mystic_staff", "item_ultimate_orb", "item_void_stone_datadriven"},
	{"item_orchid", "item_recipe_orchid", "item_oblivion_staff_datadriven", "item_oblivion_staff_datadriven"},
	{"item_cyclone", "item_recipe_cyclone", "item_staff_of_wizardry", "item_void_stone_datadriven", "item_sobi_mask_datadriven"},
	{"item_force_staff_datadriven", "item_recipe_force_staff_datadriven", "item_staff_of_wizardry", "item_ring_of_regen"},
	{"item_dagon_datadriven", "item_recipe_dagon_datadriven", "item_staff_of_wizardry", "item_null_talisman_datadriven"},
	{"item_dagon_2_datadriven", "item_recipe_dagon_datadriven", "item_dagon_datadriven"},
	{"item_dagon_3_datadriven", "item_recipe_dagon_datadriven", "item_dagon_2_datadriven"},
	{"item_dagon_4_datadriven", "item_recipe_dagon_datadriven", "item_dagon_3_datadriven"},
	{"item_dagon_5_datadriven", "item_recipe_dagon_datadriven", "item_dagon_4_datadriven"},
	{"item_refresher_datadriven", "item_recipe_refresher_datadriven", "item_pers_datadriven", "item_oblivion_staff_datadriven"},
	{"item_assault_lua", "item_recipe_assault_lua", "item_platemail", "item_hyperstone", "item_chainmail"},
	{"item_heart_datadriven", "item_recipe_heart_datadriven", "item_vitality_booster", "item_reaver"},
	{"item_black_king_bar_datadriven", "item_recipe_black_king_bar_datadriven", "item_ogre_axe", "item_mithril_hammer"},
	{"item_shivas_guard", "item_recipe_shivas_guard", "item_platemail", "item_mystic_staff"},
	{"item_bloodstone_datadriven", "item_recipe_bloodstone_datadriven", "item_soul_ring_datadriven", "item_soul_booster_datadriven"},
	{"item_sphere", "item_recipe_sphere", "item_ultimate_orb", "item_pers_datadriven"},
	{"item_vanguard_lua", "item_ring_of_health", "item_vitality_booster", "item_stout_shield"},
	{"item_crimson_guard_lua", "item_recipe_crimson_guard_lua", "item_vanguard_lua", "item_buckler_2"},
	{"item_blade_mail", "item_broadsword", "item_chainmail", "item_robe"},
	{"item_hood_of_defiance_datadriven", "item_ring_of_health", "item_cloak", "item_ring_of_regen", "item_ring_of_regen"},
	{"item_monkey_king_bar_datadriven", "item_demon_edge", "item_javelin_datadriven", "item_javelin_datadriven"},
	{"item_radiance", "item_recipe_radiance", "item_relic"},
	{"item_butterfly_datadriven", "item_eagle", "item_talisman_of_evasion", "item_quarterstaff"},
	{"item_basher", "item_recipe_basher", "item_javelin_datadriven", "item_belt_of_strength"},
	{"item_bfury_datadriven", "item_broadsword", "item_claymore", "item_pers_datadriven"},
	{"item_yasha", "item_recipe_yasha", "item_blade_of_alacrity", "item_boots_of_elves"},
	{"item_manta", "item_recipe_manta", "item_yasha", "item_ultimate_orb"},
	{"item_lesser_crit", "item_recipe_lesser_crit", "item_broadsword", "item_blades_of_attack"},
	{"item_armlet", "item_recipe_armlet", "item_helm_of_iron_will", "item_gloves", "item_blades_of_attack"},
	{"item_invis_sword", "item_shadow_amulet_datadriven", "item_claymore"},
	{"item_sange_and_yasha_datadriven", "item_yasha", "item_sange_datadriven"},
	{"item_satanic_datadriven", "item_recipe_satanic_datadriven", "item_helm_of_the_dominator_datadriven", "item_reaver"},
	{"item_mjollnir_datadriven", "item_recipe_mjollnir_datadriven", "item_maelstrom_datadriven", "item_hyperstone"},
	{"item_skadi_datadriven", "item_ultimate_orb", "item_ultimate_orb", "item_point_booster", "item_orb_of_venom_lua"},
	{"item_sange_datadriven", "item_recipe_sange_datadriven", "item_ogre_axe", "item_belt_of_strength"},
	{"item_helm_of_the_dominator_datadriven", "item_helm_of_iron_will", "item_lifesteal_datadriven"},
	{"item_maelstrom_datadriven", "item_recipe_maelstrom_datadriven", "item_gloves", "item_mithril_hammer"},
	{"item_desolator_datadriven", "item_recipe_desolator_datadriven", "item_mithril_hammer", "item_mithril_hammer"},
	{"item_mask_of_madness_datadriven", "item_recipe_mask_of_madness_datadriven", "item_lifesteal_datadriven"},
	{"item_diffusal_blade_datadriven", "item_recipe_diffusal_blade_datadriven", "item_blade_of_alacrity", "item_blade_of_alacrity", "item_robe"},
	{"item_diffusal_blade_2_datadriven", "item_recipe_diffusal_blade_datadriven", "item_diffusal_blade_datadriven"},
	{"item_ethereal_blade", "item_eagle", "item_ghost"},
	{"item_soul_ring_datadriven", "item_recipe_soul_ring_datadriven", "item_sobi_mask_datadriven", "item_ring_of_regen"},
	{"item_arcane_boots", "item_boots", "item_energy_booster"},
	{"item_medallion_of_courage", "item_recipe_medallion_of_courage", "item_chainmail", "item_sobi_mask_datadriven"},
	{"item_veil_of_discord_datadriven", "item_recipe_veil_of_discord_datadriven", "item_helm_of_iron_will", "item_null_talisman_datadriven"},
	{"item_rod_of_atos_datadriven", "item_staff_of_wizardry", "item_staff_of_wizardry", "item_vitality_booster"},
	{"item_abyssal_blade", "item_basher", "item_relic"},
	{"item_heavens_halberd_datadriven", "item_sange_datadriven", "item_talisman_of_evasion"},
	{"item_tranquil_boots_datadriven", "item_boots", "item_ring_of_regen", "item_ring_of_protection"},
	{"item_ancient_janggo_datadriven", "item_recipe_ancient_janggo_datadriven", "item_bracer_datadriven", "item_robe"},
	{"item_ancient_janggo_datadriven", "item_recipe_ancient_janggo_datadriven", "item_ancient_janggo_datadriven"},
	{"item_power_treads", "item_boots", "item_gloves", "item_belt_of_strength"},
	{"item_power_treads", "item_boots", "item_gloves", "item_boots_of_elves"},
	{"item_power_treads", "item_boots", "item_gloves", "item_robe"},
	{"item_ultimate_scepter", "item_point_booster", "item_ogre_axe", "item_blade_of_alacrity", "item_staff_of_wizardry"},
	{"item_soul_booster_datadriven", "item_energy_booster", "item_vitality_booster", "item_point_booster"},
	{"item_greater_crit", "item_recipe_greater_crit", "item_lesser_crit", "item_demon_edge"},
	{"item_rapier", "item_demon_edge", "item_relic"},
	{"item_bracer_datadriven", "item_recipe_bracer_datadriven", "item_circlet", "item_gauntlets"},
	-- 688
	{"item_bloodthorn_lua", "item_recipe_bloodthorn_lua", "item_orchid", "item_lesser_crit"},
	{"item_silver_edge_datadriven", "item_recipe_silver_edge_datadriven", "item_invis_sword", "item_ultimate_orb"},
	{"item_iron_talon_lua", "item_recipe_iron_talon_lua", "item_quelling_blade_lua", "item_ring_of_protection"}
}

local function isWardItem(item)
	return item:GetName() == "item_ward_sentry" or item:GetName() == "item_ward_observer" or item:GetName() == "item_ward_dispenser"
end

local function hasEmptyItemSlotForItem(hero, item_to_add)
	for i=DOTA_ITEM_SLOT_1,DOTA_ITEM_SLOT_6 do
		local item = hero:GetItemInSlot(i)
		if item == nil then
			return true
		else
			if item:GetName() == item_to_add:GetName() and item:IsStackable() then return true end
			if isWardItem(item) and isWardItem(item_to_add) then return true end
		end
	end
	return false
end

local function findCombinedItemIngrediants(courier, hero)
	local ingrediants = {}
	for i=DOTA_ITEM_SLOT_1,DOTA_ITEM_SLOT_9 do
		local item = courier:GetItemInSlot(i)
		if item ~= nil and item:GetPurchaser() == hero and not item:IsCombineLocked() then
			table.insert(ingrediants, item)
		end
	end
	for i=DOTA_ITEM_SLOT_1,DOTA_ITEM_SLOT_6 do
		local item = hero:GetItemInSlot(i)
		if item ~= nil and item:GetPurchaser() == hero and not item:IsCombineLocked() then
			table.insert(ingrediants, item)
		end
	end
	return ingrediants
end

local function transferItem(courier, hero)
	--print("transferItem " .. courier:GetName() .. " " .. hero:GetName())
	-- combine items
	local should_find_item_to_combine = true
	while should_find_item_to_combine do
		should_find_item_to_combine = false
		local ingrediants = findCombinedItemIngrediants(courier, hero)
		if #ingrediants == 0 then
			break
		end
		for i=1,#ITEM_RECIPE_RULES do
			ingrediants = findCombinedItemIngrediants(courier, hero)
			local target_item = ITEM_RECIPE_RULES[i][1]
			local can_combine = false
			local ingrediants_cache = {}
			for j=2,#ITEM_RECIPE_RULES[i] do
				local matchingIngrediantIndex = 0
				for k=1,#ingrediants do
					if ingrediants[k]:GetName() == ITEM_RECIPE_RULES[i][j] then
						matchingIngrediantIndex = k
						table.insert(ingrediants_cache, ingrediants[k])
						break
					end
				end
				if matchingIngrediantIndex > 0 then
					table.remove(ingrediants, matchingIngrediantIndex)
					if j == #ITEM_RECIPE_RULES[i] then
						--print("transferItem can combine item " .. target_item)
						can_combine = true
					end
				else
					break
				end
			end
			if can_combine then
				if target_item == "item_magic_wand" then
					local stick_charges = 0
					for j=1,#ingrediants_cache do
						if ingrediants_cache[j]:GetName() == "item_magic_stick" then
							stick_charges = ingrediants_cache[j]:GetCurrentCharges()
							break
						end
					end
					for j=1,#ingrediants_cache do
						ingrediants_cache[j]:Destroy()
					end
					local item = hero:AddItemByName(target_item)
					if stick_charges >= 1 then
						item:SetCurrentCharges(stick_charges)
					end
				else
					for j=1,#ingrediants_cache do
						ingrediants_cache[j]:Destroy()
					end
					hero:AddItemByName(target_item)
				end
				should_find_item_to_combine = true
				break
			end
		end
	end
	-- for each item in courier, move to hero if there is empty space
	for i=DOTA_ITEM_SLOT_1,DOTA_ITEM_SLOT_9 do
		local item = courier:GetItemInSlot(i)
		if item ~= nil and item:GetPurchaser() == hero and not item:IsCombineLocked() and hasEmptyItemSlotForItem(hero, item) then
			courier:MoveToNPCToGiveItem(hero, item)
            return true
		end
	end
    return false
end

local function needTransferItem(courier, hero)
	--print("needTransferItem " .. courier:GetName() .. " " .. hero:GetName())
	-- if courier has item owned by the hero or in base with hero's items in stash
	if not hero:IsAlive() then return false end
	for i=DOTA_ITEM_SLOT_1,DOTA_ITEM_SLOT_9 do
		local item = courier:GetItemInSlot(i)
		if item ~= nil and item:GetPurchaser() == hero and not item:IsCombineLocked() then
			return true
		end
	end
	if courier:IsInRangeOfShop(DOTA_SHOP_HOME, true) then
		--print("needTransferItem courier in base")
		for i=DOTA_STASH_SLOT_1,DOTA_STASH_SLOT_6 do
			local item  = hero:GetItemInSlot(i)
			if item ~= nil and not item:IsCombineLocked() then
				return true
			end
		end
	end
end

local function getFountain(team)
	if team == DOTA_TEAM_GOODGUYS then
		return Vector(-7093, -6542)
	else
		return Vector(7015, 6534)
	end
end

courier_transfer_items_lua = class({})

function courier_transfer_items_lua:GetIntrinsicModifierName()
	return "modifier_courier_transfer_items_lua"
end

function courier_transfer_items_lua:OnSpellStart()
	if not IsServer() then return end
	local courier = self:GetCaster()
	local hero = EntIndexToHScript(courier.target_hero)
	if needTransferItem(courier, hero) then
		--if courier in base, move items from heroes stash to courier
		if courier:IsInRangeOfShop(DOTA_SHOP_HOME, true) then
			for i=DOTA_STASH_SLOT_1,DOTA_STASH_SLOT_6 do
				local item  = hero:GetItemInSlot(i)
				if item ~= nil and not item:IsCombineLocked() and hasEmptyItemSlotForItem(courier, item) then
					courier:AddItem(hero:TakeItem(item))
				end
			end
		end

		courier:MoveToPosition(hero:GetAbsOrigin())
		local modifier = courier:FindModifierByName("modifier_courier_transfer_items_active_lua")
		if modifier ~= nil then
			courier:RemoveModifierByName("modifier_courier_transfer_items_active_lua")
		end
		courier:AddNewModifier(courier, self, "modifier_courier_transfer_items_active_lua", {
			target_hero = hero:GetEntityIndex()
		})

		CustomGameEventManager:Send_ServerToTeam(courier:GetTeam(),
												 "courier_start_transfer",
												 {
													id = tostring(courier:GetEntityIndex()),
													player_id = hero:GetPlayerID()
												 })
	end
end

modifier_courier_transfer_items_lua = class({})

function modifier_courier_transfer_items_lua:DeclareFunctions()
	return {
		MODIFIER_EVENT_ON_ORDER
	}
end

function modifier_courier_transfer_items_lua:OnOrder(event)
--	print("modifier_courier_transfer_items_lua:OnOrder")
--	print(event.issuer_player_index)
   -- DeepPrintTable(event)
	if not IsServer() then return end
	local courier = event.unit
	if courier ~= self:GetParent() then
		return
	end
	local ability = event.ability
	if ability == nil or ability:GetName() ~= "courier_transfer_items_lua" then
		return
	end
	local issuer_player_id = event.issuer_player_index
	local hero = PlayerResource:GetPlayer(issuer_player_id):GetAssignedHero()
	--TODO handle abilit trigger on spell start
	-- if item need transfer, issue move to player hero command & add transfering to hero modifier to the courier
	courier.target_hero = hero:GetEntityIndex()
end

function modifier_courier_transfer_items_lua:IsHidden()
	return true
end


modifier_courier_transfer_items_active_lua = class({})

function modifier_courier_transfer_items_active_lua:OnCreated(data)
	if IsServer() then
		self.data = data
		self:StartIntervalThink(0.1)
	end
end

local function go_back_to_fountain(courier)
	local fountainLocation = getFountain(courier:GetTeam())
	courier:MoveToPosition(fountainLocation)
	CustomGameEventManager:Send_ServerToTeam(
		courier:GetTeam(), "courier_end_transfer", { id = tostring(courier:GetEntityIndex()) })
	courier:RemoveModifierByName("modifier_courier_transfer_items_active_lua")
end

function modifier_courier_transfer_items_active_lua:OnIntervalThink()
	local hero = EntIndexToHScript(self.data.target_hero)
	local courier = self:GetParent()
	if (hero:GetAbsOrigin() - courier:GetAbsOrigin()):Length2D() <= 200 then
		local has_issue_command = transferItem(courier, hero)
        if not has_issue_command then
	        hero:EmitSound("General.CourierGivesItem")
		    go_back_to_fountain(courier)
        end
	elseif hero:IsAlive() then
		courier:MoveToPosition(hero:GetAbsOrigin())
	else
		go_back_to_fountain(courier)
	end
end

function modifier_courier_transfer_items_active_lua:DeclareFunctions()
	return {
		MODIFIER_EVENT_ON_ORDER
	}
end

function modifier_courier_transfer_items_active_lua:OnOrder(event)
	if not IsServer() then return end
	if event.unit ~= self:GetParent() then
		return
	end
	if event.issuer_player_index == -1 and event.order_type == DOTA_UNIT_ORDER_MOVE_TO_POSITION then
		-- order executed by ability itself
		return
	end

	local courier = event.unit
	if courier:HasModifier("modifier_courier_transfer_items_active_lua") then
		local issuer_player_id = event.issuer_player_index
		local hero = PlayerResource:GetPlayer(issuer_player_id):GetAssignedHero()
		if courier.target_hero ~= hero:GetEntityIndex() then
			courier.prev_target_hero = courier.target_hero
			print("Setting courier prev target as " .. courier.prev_target_hero)
		end
	end

	if event.ability == nil or not (event.ability:GetName() == "courier_transfer_items_lua" 
							     or event.ability:GetName() == "courier_burst_datadriven") then
		CustomGameEventManager:Send_ServerToTeam(
			event.unit:GetTeam(), "courier_end_transfer", { id = tostring(event.unit:GetEntityIndex()) })
		self:GetParent():RemoveModifierByName("modifier_courier_transfer_items_active_lua")
	end
end

function handleTransferToOther(event)
	local caster = event.caster
	local target_hero = caster.prev_target_hero
	if target_hero == nil then
		print("target hero nil")
		return
	end
	local target_hero_entity = EntIndexToHScript(target_hero)
	if not target_hero_entity:IsAlive() then
		print("target hero death")
		return
	end
	local has_item_to_transfer = false
	local courier = caster
	for i=DOTA_ITEM_SLOT_1,DOTA_ITEM_SLOT_9 do
		local item = courier:GetItemInSlot(i)
		if item ~= nil and item:GetPurchaser() == target_hero_entity and not item:IsCombineLocked() then
			print("Has item to transfer " .. item:GetName())
			has_item_to_transfer = true
		end
	end
	if not has_item_to_transfer then
		print("courier no item")
		return
	end
	caster:CastAbilityNoTarget(
		caster:FindAbilityByName("courier_transfer_items_lua"), 
		target_hero_entity:GetPlayerID())
end
