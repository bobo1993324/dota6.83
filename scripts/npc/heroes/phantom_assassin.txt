"DOTAAbilities"
{
	"Version"	"1"
// Rewrite of the Phantom Assassin Stifling Dagger ability
// Author: Pizzalol
// Date: 21.12.2014.
// Changed: 05.01.2015.
	"phantom_assassin_stifling_dagger_datadriven"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_datadriven"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_UNIT_TARGET"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_PURE"	
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"
		"AbilityTextureName"			"phantom_assassin_stifling_dagger"
	
		// Precache
		//-------------------------------------------------------------------------------------------------------------
		"precache"
		{
			"soundfile"			"soundevents/game_sounds_heroes/game_sounds_phantom_assassin.vsndevts"
			"particle"			"particles/units/heroes/hero_phantom_assassin/phantom_assassin_stifling_dagger.vpcf"
			"particle"			"particles/units/heroes/hero_phantom_assassin/phantom_assassin_stifling_dagger_debuff.vpcf"
		}
	
		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"1200"
		"AbilityCastPoint"				"0.3 0.3 0.3 0.3"
		
		// Time		
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"6"
		"AbilityDuration"				"1.0 2.0 3.0 4.0"
	
		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"30 25 20 15"
		
		//Damage
		"AbilityDamage"					"60 100 140 180"
			
		// Special	
		//-------------------------------------------------------------------------------------------------------------
		"AbilityValues"
		{
				"move_slow"					"-50"
				"dagger_speed"			"1200"
				"hero_dmg_pct"			"50"
		}
	
		"OnSpellStart"
		{
			"TrackingProjectile"
			{
				"Target"           "TARGET"
			    "EffectName"       "particles/units/heroes/hero_phantom_assassin/phantom_assassin_stifling_dagger.vpcf"
			    "Dodgeable"        "1"
			    "ProvidesVision"   "1"
			    "VisionRadius"     "300"
			    "MoveSpeed"        "%dagger_speed"
			    "SourceAttachment" "DOTA_PROJECTILE_ATTACHMENT_ATTACK_1"
			}
	
			"FireSound"
			{
				"EffectName"		"Hero_PhantomAssassin.Dagger.Cast"
				"Target"			"CASTER"
			}

			"RunScript"
			{
				"ScriptFile"	"items/item_magic_stick.lua"
				"Function"		"ProcsMagicStick"
			}
		}
	
		"OnProjectileHitUnit"
		{
			"RunScript"
			{
				"ScriptFile"	"heroes/hero_phantom_assassin/stifling_dagger.lua"
				"Function"		"handleProjectileHitUnit"
			}
		}
	
		"Modifiers"
		{
			"modifier_stifling_dagger_slow_datadriven"
			{
				"IsDebuff"	"1"
				"Duration"	"%AbilityDuration"
	
				"EffectName"		"particles/units/heroes/hero_phantom_assassin/phantom_assassin_stifling_dagger_debuff.vpcf"
				"EffectAttachType"	"follow_origin"
				"IsPurgable"		"1"
	
				"Properties"
				{
					"MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE"		"%move_slow"
				}
			}
		}
	}
	
}
