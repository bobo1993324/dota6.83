root_modifiers = {}
root_modifiers["modifier_frost_bite_root_datadriven"] = true
root_modifiers["modifier_searing_chains_debuff_datadriven"] = true
root_modifiers["modifier_dark_troll_warlord_ensnare"] = true
root_modifiers["modifier_meepo_earthbind"] = true
root_modifiers["modifier_ensnare_datadriven"] = true
root_modifiers["modifier_oracle_fortunes_end_purge"] = true
root_modifiers["modifier_lone_druid_spirit_bear_entangle_effect"] = true
root_modifiers["modifier_treant_overgrowth_datadriven"] = true
root_modifiers["modifier_abyssal_underlord_pit_of_malice_ensare_lua"] = true

ethereal_modifiers = {}
ethereal_modifiers["modifier_pugna_decrepify"] = true
ethereal_modifiers["modifier_ghost_state"] = true
ethereal_modifiers["modifier_item_ethereal_blade_ethereal"] = true

disarm_modifiers = {}
disarm_modifiers["modifier_item_heavens_halberd_datadriven_disarm"] = true
disarm_modifiers["modifier_oracle_fates_edict_lua"] = true
disarm_modifiers["modifier_elder_titan_earth_splitter_disarm"] = true
