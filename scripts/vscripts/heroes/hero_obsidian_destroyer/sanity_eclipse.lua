--[[Sanity Eclipse
	Author: chrislotix
	Date: 08.01.2015.
	]]


function SanityEclipseDamage( keys )
	
	local caster = keys.caster
	local target = keys.target
	local ability = keys.ability
	local od_int = caster:GetIntellect(true)
	local target_int = target:GetIntellect(true)
	local mana = target:GetMaxMana()
	local dmg_multiplier = ability:GetLevelSpecialValueFor("damage_multiplier", (ability:GetLevel() -1))
	local threshold = ability:GetLevelSpecialValueFor("int_threshold", (ability:GetLevel() -1))

	
	local damageDelay = 0
	if target:IsInvulnerable() or target:IsOutOfGame() then
		print(target:GetName() .. " is out of the game")
		if not target:HasModifier("modifier_astral_imprisonment_datadriven") then
			print("not imprison")
			return
		end
		damageDelay = target:FindModifierByName("modifier_astral_imprisonment_datadriven"):GetRemainingTime() + 0.1
		print(damageDelay)
	end

	
	target:SetThink(function()
		local damage_table = {} 

		damage_table.attacker = caster
		damage_table.damage_type = ability:GetAbilityDamageType()
		damage_table.ability = ability
		damage_table.victim = target

		--if od's int is lower than targets int then keep do nothing and keep targets mana as it is
		if od_int < target_int then
			if caster:HasScepter() then
				target:Script_ReduceMana(target:GetMana() * 0.75, ability)
			end
			return
		end
			--if the int difference is below or equal to threshold, burn 75% current mana and apply int difference * damage_modifier in magic damage
		if (od_int - target_int) <= threshold or caster:HasScepter() then
			target:Script_ReduceMana(target:GetMana() * 0.75, ability)
		end
		if not target:IsMagicImmune() then
			damage_table.damage = (od_int - target_int)	* dmg_multiplier
			ApplyDamage(damage_table)
		end
	end, "sanity eclipse", damageDelay)
end


	







	


	
