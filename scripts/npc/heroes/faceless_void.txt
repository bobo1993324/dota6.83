"DOTAAbilities"
{
	"Version"	"1"
	// Rewrite of the Faceless Void Backtrack ability
	// Author: Pizzalol
	// Date: February 14, 2016
	// Version: 6.85
	// Type: Datadriven
	//
	// ----- FILE REQUIREMENTS -----
	// Script files:
	// scripts/vscripts/heroes/hero_faceless_void/backtrack.lua
	"faceless_void_backtrack_datadriven"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_datadriven"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE"
		"AbilityTextureName"			"faceless_void_backtrack"
	
		// Precache
		//-------------------------------------------------------------------------------------------------------------
		"precache"
		{
			"particle"			"particles/units/heroes/hero_faceless_void/faceless_void_backtrack.vpcf"
		}
		
		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilityValues"
		{
				"dodge_chance_pct"		"10 15 20 25"
		}
	
		"Modifiers"
		{
			"modifier_backtrack_datadriven"
			{
				"Passive"	"1"
				"IsHidden"	"1"
	
				"OnTakeDamage"
				{
					"RunScript"
					{
						"ScriptFile"	"heroes/hero_faceless_void/backtrack.lua"
						"Function"		"handleDamageTaken"
						"Damage"		"%attack_damage"
					}
				}
			}			
		}
	}
	//=================================================================================================================
	// Faceless Void: Chronosphere Rewrite
	//=================================================================================================================
	"faceless_void_chronosphere_lua"
	{
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_faceless_void/chronosphere.lua"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_AOE | DOTA_ABILITY_BEHAVIOR_POINT"
		"MaxLevel"						"3"
		"RequiredLevel"					"6"
		"LevelsBetweenUpgrades"			"5"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_YES"
		"SpellDispellableType"			"SPELL_DISPELLABLE_NO"
		"FightRecapLevel"				"2"
		"HasScepterUpgrade"				"1"

		// Ability Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"600"
		"AbilityCastPoint"				"0.35"
		"AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_4"
		"AbilityProcsMagicStick"		"1"

		// Ability Resource
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"150 225 300"

		// Ability Multimedia
		//-------------------------------------------------------------------------------------------------------------
		"AbilityTextureName"			"faceless_void_chronosphere"
		"AbilitySound"					"Hero_FacelessVoid.Chronosphere"

		// Stats
		//-------------------------------------------------------------------------------------------------------------
		"AbilityModifierSupportBonus"		"50"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilityValues"
		{
			"radius"				"425"
			"duration"
			{
				"value"			"4.0 4.5 5.0"
				"special_bonus_scepter"	"+0 +0.5 +1"
			}
			"bonus_attack_speed"	"0"
			"vision_radius"			"475"
			"AbilityCooldown"
			{
				"value"					"130 115 100"
				"special_bonus_scepter"	"=60"
			}
		}

		"precache"
		{
			"soundfile"	"soundevents/game_sounds_heroes/game_sounds_faceless_void.vsndevts"
		}
	}
}
