# dota6.83 - dota2自定义游戏

#### 介绍
Dota6.83版本移植，养老dota，best dota。

特性：

- AP/RD模式, 默认AP，准备阶段公屏输入'-rd'切换RD模式，输入'-sp'可以选择相同英雄，输入'-cm'延长英雄选择时间
- 准备阶段公屏输入'-vsbot'切换BOT模式，在夜宴生成5个难度为Hard的Bot，免费鸡，无拉野机制，天辉无Bot
- AP模式在英雄选择和策略阶段可以输入'-repick'重新选择英雄，-100金钱
- 使用6.83英雄池，去除天赋，去除魔晶，全部英雄技能调整
- 英雄最高25级，使用6.83版本升级经验
- 使用6.83版本道具，不掉落中立道具。
- 吸血，疯脸，支配，撒旦，暗灭，散失为法球效果，不能互相叠加
- 英雄法球机制，无法与物品法球叠加
- 使用重写绘制的6.83地图，去除骑兵，去除塔回血加甲光环。
- 使用6.83版本属性加成
- 使用6.83野区经济经验，30秒刷第一波野，去除远古旁边的大野
- 使用6.83英雄击杀助攻金钱计算公式
- 使用6.82英雄复活时间 = 4 * 英雄等级
- 信使需要购买，并且用飞行信使物品升级。
- 去除英雄背包
- 去除近战英雄内置伤害格挡，可以购买圆盾
- 重写塔防，塔防CD在一塔倒了之后刷新，塔防开启期间小兵没有无敌，没有溅射效果，塔防CD在泉水查看。
- 去除扫描
- 赏金符给个人经济经验，不再提供团队经济
- 随机英雄提供200金币
- 死后不提供免费TP
- 调整边路小兵相遇位置
- 小兵野怪打断净化、大药
- 分裂纯粹伤害

mod开源：https://gitee.com/bobo1993324/dota6.83

Bug报告：https://gitee.com/bobo1993324/dota6.83/issues

SpellLibrary提供的部分自定义物品和技能的实现：https://github.com/Pizzalol/SpellLibrary

dota2ai提供了AI购买物品的代码: https://github.com/adamqqqplay/dota2ai

bot重写的代码基于Dota2-FullOverwrite修改: https://github.com/Nostrademous/Dota2-FullOverwrite

6.88g 地图基于 DOTA IMBA 在 6.88 时期使用的地图文件：https://github.com/EarthSalamander42/dota_imba