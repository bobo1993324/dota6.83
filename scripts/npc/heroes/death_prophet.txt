"DOTAAbilities"
{
	"Version"	"1"

	"death_prophet_silence_lua"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_death_prophet/silence.lua"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_AOE | DOTA_ABILITY_BEHAVIOR_POINT"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"
		"SpellDispellableType"			"SPELL_DISPELLABLE_YES"
		
		// Multimedia
		"AbilityTextureName"			"death_prophet_silence"
		"AbilitySound"					"Hero_DeathProphet.Silence"
		"AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_2"
	
		// Stats
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"900"
		"AbilityCastPoint"				"0.5"
		"AbilityProcsMagicStick"		"1"
		"AnimationPlaybackRate"			"0.856"

		// Time		
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"15"
		"AbilityDuration"				"3.0 4.0 5.0 6.0"

		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"80"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilityValues"
		{
			"radius"					"425"
			"duration"					"3.0 4.0 5.0 6.0"
		}

		"precache"
		{
			"particle" 	"particles/units/heroes/hero_death_prophet/death_prophet_silence.vpcf"
			"particle"	"particles/units/heroes/hero_death_prophet/death_prophet_silence_impact.vpcf"
			"soundfile" "soundevents/game_sounds_heroes/game_sounds_death_prophet.vsndevts"
		}
	}
}
