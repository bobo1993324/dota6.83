"DOTAAbilities"
{
	"Version"	"1"
	//=================================================================================================================
	// Enchantress: Enchant
	// Overwrite from dota_imba
	// Original code is under Apache License V2.0
	//=================================================================================================================
	"enchantress_enchant_lua"
	{
		"BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_enchantress/enchant.lua"
		"AbilityTextureName"			"enchantress_enchant"
		// General
		//-------------------------------------------------------------------------------------------------------------
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_UNIT_TARGET"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitTargetFlags"		"DOTA_UNIT_TARGET_FLAG_NOT_ANCIENTS"
		"SpellDispellableType"			"SPELL_DISPELLABLE_YES"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"
		"AbilitySound"					"Hero_Enchantress.EnchantCreep"

		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"700"
		"AbilityCastPoint"				"0.3"
		
		// Time		
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"30 25 20 15"
		"AbilityDuration"				"5.5"
		
		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"65"
			
		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilityValues"
		{
			"slow_movement_speed"		"-20 -30 -40 -50"
			"tooltip_duration"			"5.5"
			"dominate_duration"			"80.0"
		}
		"AbilityCastAnimation"		"ACT_DOTA_CAST_ABILITY_2"
	}
	"enchantress_impetus_lua"
	{
		"BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_enchantress/impetus.lua"
		"AbilityType"					"DOTA_ABILITY_TYPE_ULTIMATE"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_UNIT_TARGET | DOTA_ABILITY_BEHAVIOR_AUTOCAST | DOTA_ABILITY_BEHAVIOR_ATTACK"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_PURE"
		"FightRecapLevel"				"1"
		"AbilityTextureName"			"enchantress_impetus"
		"MaxLevel"              "3"
		"RequiredLevel"         "6"
		"LevelsBetweenUpgrades" "5"
		"HasScepterUpgrade"		"1"

		"precache"
		{
			"particle"		"particles/units/heroes/hero_enchantress/enchantress_impetus.vpcf"
			"soundfile"		"soundevents/game_sounds_heroes/game_sounds_enchantress.vsndevts"
		}
		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastPoint"				"0.0 0.0 0.0"
	
		// Time		
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"0.0"
		"AbilityDuration"				"1.5"

		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"55 60 65"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilityValues"
		{
			"distance_damage_pct"			"15.0 20.0 25.0"
			"distance_damage_cap"			"375 500 625"
			"bonus_attack_range_scepter"
			{
				"value"						"190"
				"RequireScepter"			"1"
			}
			"AbilityCastRange"
			{
				"value"						"550"
				"special_bonus_scepter"		"+190"
			}
		}
	}
	"enchantress_aghs_lua"
	{
		"BaseClass"				"ability_lua"
		"ScriptFile"			"heroes/hero_enchantress/aghs.lua"
		"Innate"				"1"
		"AbilityBehavior"		"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_NOT_LEARNABLE | DOTA_ABILITY_BEHAVIOR_HIDDEN"
		"MaxLevel"				"1"
	}
}
