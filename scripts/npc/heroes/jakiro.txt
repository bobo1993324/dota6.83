"DOTAAbilities"
{
	"Version"	"1"
	
	"jakiro_liquid_fire_lua"
	{
		"BaseClass"						"ability_lua"
		"ScriptFile"					"heroes/hero_jakiro/liquid_fire.lua"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_UNIT_TARGET | DOTA_ABILITY_BEHAVIOR_AUTOCAST | DOTA_ABILITY_BEHAVIOR_ATTACK"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_MAGICAL"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC | DOTA_UNIT_TARGET_BUILDING"
		"AbilityUnitTargetFlags"		"DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_YES"
		"SpellDispellableType"			"SPELL_DISPELLABLE_YES"
		"AbilitySound"					"Hero_Jakiro.LiquidFire"
		"AbilityTextureName"			"jakiro_liquid_fire"

		"precache"
		{
			"soundfile"	"soundevents/game_sounds_heroes/game_sounds_jakiro.vsndevts"
			"particle"	"particles/units/heroes/hero_jakiro/jakiro_base_attack_fire.vpcf"
			"particle"	"particles/units/heroes/hero_jakiro/jakiro_liquid_fire_explosion.vpcf"
			"particle"	"particles/units/heroes/hero_jakiro/jakiro_liquid_fire_debuff.vpcf"
		}

		// Stats
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"20 15 10 4"
		"AbilityDuration"				"5.0"
		"AbilityCastRange"				"600"
		"AbilityCastPoint"				"0.0 0.0 0.0 0.0"

		// Stats
		//-------------------------------------------------------------------------------------------------------------
		"AbilityModifierSupportBonus"		"35"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilityValues"
		{
			"slow_attack_speed_pct" 		"-20 -30 -40 -50"
			"radius"				"300"
			"damage"				"12 16 20 24"
			"building_dmg_pct"		"100"
		}
		"AbilityCastAnimation"		"ACT_DOTA_CAST_ABILITY_3"
	}
}